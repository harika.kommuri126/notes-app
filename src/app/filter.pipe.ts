import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(data: any[], query?: any, key?: any): any {
    if (!data || data.length === 0) {
      return data;
    }

    if (!query) {
      return data;
    }

    let keys = Object.keys(data[0]);
    return data.filter(obj => {
      for (let i = 0; i < keys.length; i++) {
        if (key && keys[i] !== key) {
          continue;
        }

        if (obj[keys[i]]) {
          if (obj[keys[i]].toString().toLowerCase().indexOf(query.toLowerCase()) > -1) {
            return obj;
          }
        }
      }
    });
  }
}
