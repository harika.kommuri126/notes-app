import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NotesAppComponent} from './notes-app/notes-app.component';

const notesAppRoutes: Routes = [
  {
    path: 'notes', component: NotesAppComponent
  }
];

const routes: Routes = [
  ...notesAppRoutes,
  { path: '', redirectTo: 'notes', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
