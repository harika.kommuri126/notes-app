import {Component, OnInit, Injector } from '@angular/core';
import { FilterPipe } from '../filter.pipe';

@Component({
  selector: 'app-notes-app',
  templateUrl: './notes-app.component.html',
  styleUrls: ['./notes-app.component.scss'],
  providers: [FilterPipe]
})
export class NotesAppComponent implements OnInit {
  private filter: FilterPipe = this.injector.get(FilterPipe);
  notes: any;
  note: any;
  selectedNote: number;
  searchTerm: string;
  toggle: boolean;

  constructor(private injector: Injector) {
    this.selectedNote = null;
    this.notes = [];
    this.note = '';
    this.searchTerm = '';
    this.toggle = true;
  }

  ngOnInit(): void {
    localStorage.setItem('notes', JSON.stringify([]));
  }

  searchNotes(): void {
    this.notes = JSON.parse(localStorage.getItem('notes'));
    if (this.searchTerm) {
      this.notes = this.filter.transform(this.notes, this.searchTerm);
    }
  }

  updateNote(): void {
    if (this.selectedNote !== null) {
      this.notes[this.selectedNote] = this.note;
      localStorage.setItem('notes', JSON.stringify(this.notes));
    }
  }

  addNote(): void {
    this.initInputs();
    this.notes.push(this.note);
    this.selectedNote = this.notes.length - 1;
    localStorage.setItem('notes', JSON.stringify(this.notes));
  }

  initInputs(): void {
    this.note = {
      header: '',
      description: '',
      time: Date.now(),
      index: this.notes ? this.notes.length : 0
    };
  }

  toggleSideBar(inputType?): void {
    this.toggle = !this.toggle;
    if (inputType === 'search') {
      setTimeout(() => {
        document.getElementById('search').focus();
      });
    }
  }

  deleteNote(index): void {
    this.notes.splice(index, 1);
    if (index === this.note.index || !this.notes.length) {
      this.note = '';
    }
    for (let i = 0; i < this.notes.length; i++) {
      if (this.note && this.notes[i].index === this.note.index) {
        this.note.index = i;
      }
      this.notes[i].index = i;
    }
    localStorage.setItem('notes', JSON.stringify(this.notes));
    this.selectedNote = null;
  }

  editNote(index): void {
    this.selectedNote = index;
    this.note = this.notes[index];
  }

  selectNote(noteIndex): void {
    this.selectedNote = noteIndex;
    this.note = this.notes[noteIndex];
  }

}
